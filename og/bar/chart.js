// Get data
d3.json("data.json", function (error, data) {
    // Handle error
    if (error != null) {
        console.error(error);
        return;
    }
    
    /**
     * Filters
     */
    // Set selectors
    var selectBox = d3.select("#filter")
        .append("select")
        .attr({})
        .on({
            "change" : function () {
                var key = this.value;
                if (key != "") {
                    plotGraph(key)
                }
            }
        });
    
    //options
    selectBox.selectAll("options")
        .data(function () {
            return Object.keys(data).filter(function (d) {
                return (d != "students");
            });
        })
        .enter()
        .append("option")
        .attr({
            "value" : function (d) {
                return d;
            }
        })
        .text(function (d) {
            return d;
        });
    
    
    /**
     * Chart
     */
    var plotGraph = function (key) {
        // default key
        key  = (key !== undefined) ? key : "classes";
        
        // Singular name of key
        var keySingular;
        switch (key) {
            case "classes" : 
                keySingular = "class";
                break;
            case "forms" : 
                keySingular = "form";
                break;
            case "programs" :
                keySingular = "program";
                break;
        }
        
        // Prepare data
        var prepareDataKey = function () {
            //
            data[key].forEach(function (keyDetails) {
                var filteredData = data.students.filter(function (student) {
                    return (student[keySingular] == keyDetails.id) ? true : false;
                });

                keyDetails["students"] = filteredData;
            });
        };

        // Run data prepare method
        prepareDataKey();
        var plotData = data[key];
        console.log(plotData);
        
        // Attributes
        var margins = {
                top : 30,
                right : 30,
                bottom : 30,
                left : 30
            },
            canvasWidth = 600,
            canvasHeight = 600,
            containerWidth = canvasWidth - (margins.left + margins.right),
            containerHeight = canvasHeight - (margins.top + margins.bottom),
            barPadding = 10,
            barWidth = (containerWidth - (plotData.length * barPadding)) / plotData.length,
            barColor = "steelblue",
            barColorActive = "orangeblue";

        // Round to nearest 10
        function roundUp (number) {
            return Math.round(number / 10) * 10;
        }

        // Scales
        var xScale = d3.scale.ordinal()
                .rangeBands([0, containerWidth])
                .domain(plotData.map(function (d) {
                        return d.name;
                    })
                ),
            yScale = d3.scale.linear()
                .range([0 , containerHeight])
                .domain([roundUp(d3.max(plotData, function (d) {
                        return d.students.length;
                    })), 0]
                ),
            colorScales = d3.scale.category20(),
            colorScalesActive = d3.scale.category20b();

        // Tooltip
        var toolTip = d3.tip()
            .attr({
                "class" : "d3-tip" 
            })
            .offset([-10, 0])
            .html(function (d) {
                return "<strong>Total Students</strong>: <span style='color: brown'>" + d.students.length + "</span><br />";
            });

        // Axes
        var xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("bottom"),
            yAxis = d3.svg.axis()
                .scale(yScale)
                .orient("left");

        // Canvas
        var canvas = d3.select("svg")
            .attr({
                "width" : canvasWidth,
                "height" : canvasHeight
            });

        // Append tooltip
        canvas.call(toolTip);

        // Plot axes
        canvas.selectAll("g.x.axis").remove();
        canvas.append("g")
            .attr({
                "class" : "x axis",
                "transform" : "translate(" + margins.top + "," + (containerHeight + margins.top) + ")"
            })
            .call(xAxis);

        canvas.selectAll("g.y.axis").remove();
        canvas.append("g")
            .attr({
                "class" : "y axis",
                "transform" : "translate(" + margins.left + ", " + margins.top + ")"
            })
            .call(yAxis)
            .append("text")
            .attr({
                "transform" : "rotate(-90)",
                "y" : 15, // margins.left,
                "x" : - (70 + margins.top),
                "dy" : "0.70em"
            })
            .text("No of Students");

        // Container
        canvas.selectAll("g.container").remove();
        var containerG = canvas.append("g")
            .attr({
                "class" : "container",
                "transform" : "translate(" + margins.top + "," + margins.left + ")",
                "width" : containerWidth,
                "height" : containerHeight
            });

        // Plot bars
        var barG = containerG.selectAll("g")
            .data(plotData)
            .enter()
            .append("g")
            .attr({
                "transform" : function (d, i) {
                    var xPos = i * (barWidth + barPadding);
                    return "translate(" + (xPos + barPadding)  + ", 0)"
                }
            });

        barG.append("rect")
            .attr({
                "class" : "bar",
                "width" : barWidth,
                "height" : function (d, i) {
                    var height = containerHeight - yScale(d.students.length);
                    return height;
                },
                "y" : function (d, i) {
                    return yScale(d.students.length);
                }
            })
            .style()
            .on({
                "mouseover" : function (d, i) {
                    toolTip.show(d); // Show tooltip
                },
                "mouseout" : function (d, i) {
                    toolTip.hide(d); // Hide tooltip
                },
                "click" : function (d, i) {
                    console.log("Clicked: " + d.name);
                }
            });
    }

    // Run default graph
    plotGraph();
});