// Attributes
var margins = {
        top : 30,
        right : 30,
        bottom : 30,
        left : 30
    },
    canvasWidth = 600,
    canvasHeight = 600,
    containerWidth = canvasWidth - (margins.left + margins.right),
    containerHeight = canvasHeight - (margins.top + margins.bottom);

// Canvas
var canvas = d3.select("svg")
    .attr({
        "width" : canvasWidth,
        "height" : canvasHeight
    });

// Container
var containerG = canvas.append("g")
    .attr({
        "transform" : "translate(" + margins.top + "," + margins. + "")"
    })